Strelnokoff
===========

<!--
SPDX-FileCopyrightText: (c) 2001-2024 Chris Pressey, Cat's Eye Technologies
This file is distributed under a 2-clause BSD license.
For more information, see the following file in the LICENSES directory:
SPDX-License-Identifier: LicenseRef-BSD-2-Clause-X-Strelnokoff
-->

Version 1.1
| _Try it online_ [@ catseye.tc](https://catseye.tc/installation/Strelnokoff)
| _Wiki entry_ [@ esolangs.org](https://esolangs.org/wiki/Strelnokoff)

- - - -

This is the reference distribution for Strelnokoff, an esoteric programming
language which is both imperative and non-deterministic.  That is to say,
the order in which instructions are executed is not defined.  It could even
be random.  Despite this situation, it is possible to write reasonable
programs in Strelnokoff.

### Historical Version

The original (v1.0) release of this distribution consisted of a Strelnokoff
interpreter incompletely implemented in Perl 5 (with features like array
access and input being parsed, but not implemented during execution) and with
essentially no documentation.

The Perl implementation is still included in this distribution, in the
`impl/strelnokoff.pl` subdirectory, but it is no longer considered the
reference implementation.

### Current Version

The current version of the Strelnokoff language, v1.1, is a conservative
update and largely backwards-compatible with v1.0, but in clarifying some
behaviour, it might result in breakage of existing Strelnokoff programs,
depending on what assumptions they might be making, given they have been
relying on an incomplete Perl implementation for their behaviour.

There is now an implementation of Strelnokoff in Lua 5.3.  This
implementation, along with the language documentation that has now
been written for v1.1:

*   [The Strelnokoff Programming Language](doc/)

...should be considered the canonical reference for the Strelnokoff
language.

This distribution also contains some example Strelnokoff programs
in the `eg` directory.

### License

This distribution is distributed under a constellation of permissive
licenses.  Following the [REUSE](https://reuse.software/) convention
(version 3.0), the licenses themselves are kept in the [LICENSES](LICENSES)
directory, and each file is linked to the license that applies to it,
either by comments in the file itself, or in [.reuse/dep5](.reuse/dep5).
